import Element from "./element";
import _ from "lodash";
import holidays from "./holidays";
import mui from "../vendor/mui.min";
import "../css/zyjh5DatePicker.css";


mui.ready(function () {
    (function (global, factory) {
        typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
            typeof define === 'function' && define.amd ? define(factory) :
                (global.zyjh5DatePicker = factory());
    })(window, (function () {
        'use strict';

        function zyjh5DatePicker(opts) {
            this.days_CN = opts && opts.days_CN ? ['日', '一', '二', '三', '四', '五', '六'] : opts.days_CN;
            this.init();
        }

        zyjh5DatePicker.prototype.week = function () {
            var _this = this;
            var days = [];
            for (let index = 0; index < 7; index++) {
                if (index == 0 || index == 6) {
                    days.push(Element('div', { class: 'zyjh5DatePicker-day-item zyjh5DatePicker-day-weekend' }, {}, [_this.days_CN[index]]))
                } else {
                    days.push(Element('div', { class: 'zyjh5DatePicker-day-item' }, {}, [_this.days_CN[index]]))
                }
            }
            return days;
        }

        zyjh5DatePicker.prototype.getDate = function (year, month) {
            var _this = this;
            var d = new Date(year, month, 0);
            return d.getDate();
        }

        zyjh5DatePicker.prototype.getDay = function (year, month) {
            var _this = this;
            var d = new Date(year, month, 1);
            return d.getDay();
        }

        zyjh5DatePicker.prototype.getFull = function (num, tmp_row) {
            var _this = this;
            for (let j = 0; j < num; j++) {
                tmp_row.push(Element('div', { class: 'zyjh5DatePicker-date-colum' }, {}, [
                    Element('div', { class: 'zyjh5DatePicker-date-colum-wrap' }, {}, [
                        Element('div', { class: 'zyjh5DatePicker-date-colum-wrap-date' }, {}, []),
                        Element('div', { class: 'zyjh5DatePicker-date-colum-wrap-info' }, {}, []),
                    ]),
                ]))
            }
        }

        zyjh5DatePicker.prototype.date = function () {
            var _this = this;
            var tmp = [];
            var tmp_row = [];
            var d = new Date();
            let nd = d.getDate();
            var hdstr = (d.getMonth() + 1) < 10 ? ('0' + (d.getMonth() + 1)) : (d.getMonth() + 1);
            let dates = _this.getDate(d.getFullYear(), d.getMonth());
            let day = _this.getDay(d.getFullYear(), d.getMonth());
            _this.getFull(day, tmp_row);
            for (let index = 0; index < dates; index++) {
                let classstr = 'zyjh5DatePicker-date-colum-wrap';
                if ((index + 1) === nd) {
                    classstr = 'zyjh5DatePicker-date-colum-wrap active';
                }
                let canc = true;
                if ((index + 1) < nd) {
                    classstr += ' cantchoose';
                    canc = false;
                }
                let odn = (index + 1) < 10 ? ('0' + (index + 1)) : (index + 1);
                let jr = holidays[hdstr + '-' + odn];
                let jrstatus = '';
                if (jr) {
                    if (jr.holiday) {
                        jrstatus = '休';
                    } else {
                        jrstatus = '班';
                    }
                }
                tmp_row.push(Element('div', { class: 'zyjh5DatePicker-date-colum' }, {}, [
                    Element('div', { class: (jrstatus == '休' ? (classstr + ' xiu') : (classstr + '')) }, canc ? { tap: _this.choosedate } : {}, [
                        Element('div', { class: (jr ? 'zyjh5DatePicker-date-colum-wrap-status active' : 'zyjh5DatePicker-date-colum-wrap-status') }, {}, [jrstatus]),
                        Element('div', { class: 'zyjh5DatePicker-date-colum-wrap-date' }, {}, [index + 1]),
                        Element('div', { class: 'zyjh5DatePicker-date-colum-wrap-info' }, {}, [jr && jr.holiday ? jr.name : '¥228']),
                    ]),
                ]))
            }
            tmp_row = _.chunk(tmp_row, 7);
            _this.getFull(7 - tmp_row[tmp_row.length - 1].length, tmp_row[tmp_row.length - 1]);
            _.forEach(tmp_row, function (arr) {
                tmp.push(Element('div', { class: 'zyjh5DatePicker-date-row' }, {}, arr));
            })
            return tmp;
        }

        zyjh5DatePicker.prototype.tree = function () {
            var _this = this;
            return Element('div', { id: 'zyjh5DatePicker', class: 'zyjh5DatePicker' }, {}, [
                Element('div', { class: 'zyjh5DatePicker-title' }, {}, [
                    Element('span', {}, {}, ['请选择日期']),
                    Element('div', { class: 'zyjh5DatePicker-closebox' }, { tap: _this.closezyjh5date }, [
                        Element('div', { class: 'zyjh5DatePicker-closebox-line1' }),
                        Element('div', { class: 'zyjh5DatePicker-closebox-line2' }),
                    ])
                ]),
                Element('div', { class: 'zyjh5DatePicker-day' }, {}, _this.week()),
                Element('div', { class: 'zyjh5DatePicker-yearslide' }, {}, [
                    // Element('div', { class: 'zyjh5DatePicker-yearslide-iconleft' }),
                    Element('div', { class: 'zyjh5DatePicker-yearslide-text' }, {}, ['2019年2月']),
                    // Element('div', { class: 'zyjh5DatePicker-yearslide-iconright' }),
                ]),
                Element('div', { class: 'zyjh5DatePicker-date' }, {}, _this.date()),
            ]);
        }

        zyjh5DatePicker.prototype.init = function () {
            var _this = this;
            document.body.appendChild(_this.tree().render());
            _this.registerEvt();
        }

        zyjh5DatePicker.prototype.choosedate = function (e) {
            var _this = this;
            var wraps = document.getElementsByClassName('zyjh5DatePicker-date-colum-wrap');
            _.forEach(wraps, function (item, index) {
                if (item.classList.contains('active')) {
                    item.classList.remove('active')
                }
            })
            if (e.target.classList.contains('zyjh5DatePicker-date-colum-wrap')) {
                if (e.target.classList.contains('active')) {
                    e.target.classList.remove('active')
                } else {
                    e.target.classList.add('active');
                }
            } else {
                if (e.target.parentNode.classList.contains('active')) {
                    e.target.parentNode.classList.remove('active')
                } else {
                    e.target.parentNode.classList.add('active');
                }
            }
            e.stopPropagation();
        }


        zyjh5DatePicker.prototype.closezyjh5date = function (e) {
            var _this = this;
            if (document.getElementById('zyjh5DatePicker').classList.contains('active')) {
                document.getElementById('zyjh5DatePicker').classList.remove('active')
                document.getElementById('zyjh5DatePicker-mask').classList.remove('active');
            } else {
                document.getElementById('zyjh5DatePicker').classList.add('active');
                document.getElementById('zyjh5DatePicker-mask').classList.remove('active');
            }
        }

        zyjh5DatePicker.prototype.registerEvt = function () {
            var _this = this;
            document.getElementById('zyjh5DatePicker-btn').addEventListener('tap', function (e) {
                if (document.getElementById('zyjh5DatePicker').classList.contains('active')) {
                    document.getElementById('zyjh5DatePicker').classList.remove('active')
                    document.getElementById('zyjh5DatePicker-mask').classList.remove('active');
                } else {
                    document.getElementById('zyjh5DatePicker').classList.add('active');
                    document.getElementById('zyjh5DatePicker-mask').classList.add('active');
                }
            })

            document.getElementById('zyjh5DatePicker-mask').addEventListener('tap', function (e) {
                document.getElementById('zyjh5DatePicker').classList.remove('active')
                document.getElementById('zyjh5DatePicker-mask').classList.remove('active');
                e.stopPropagation();
            });
        }

        return zyjh5DatePicker;
    }));

    var t = new zyjh5DatePicker();
    console.log(t);
});