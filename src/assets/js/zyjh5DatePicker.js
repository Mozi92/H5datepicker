import Element from "./element";
import _ from "lodash";
import holidays from "./holidays";
import mui from "../vendor/mui.min";
import "../css/zyjh5DatePicker.css";


mui.ready(function () {
    document.getElementById('zyjh5DatePicker-btn').addEventListener('tap', function (e) {
        if (document.getElementById('zyjh5DatePicker').classList.contains('active')) {
            document.getElementById('zyjh5DatePicker').classList.remove('active')
            document.getElementById('zyjh5DatePicker-mask').classList.remove('active');
        } else {
            document.getElementById('zyjh5DatePicker').classList.add('active');
            document.getElementById('zyjh5DatePicker-mask').classList.add('active');
        }
    })

    document.getElementById('zyjh5DatePicker-mask').addEventListener('tap', function (e) {
        document.getElementById('zyjh5DatePicker').classList.remove('active')
        document.getElementById('zyjh5DatePicker-mask').classList.remove('active');
        e.stopPropagation();
    })

    function choosedate(e) {
        var wraps = document.getElementsByClassName('zyjh5DatePicker-date-colum-wrap');
        _.forEach(wraps, function (item, index) {
            if (item.classList.contains('active')) {
                item.classList.remove('active')
            }
        })
        if (e.target.classList.contains('zyjh5DatePicker-date-colum-wrap')) {
            if (e.target.classList.contains('active')) {
                e.target.classList.remove('active')
            } else {
                e.target.classList.add('active');
            }
        } else {
            if (e.target.parentNode.classList.contains('active')) {
                e.target.parentNode.classList.remove('active')
            } else {
                e.target.parentNode.classList.add('active');
            }
        }
        e.stopPropagation();
    }

    function closezyjh5date(e) {
        if (document.getElementById('zyjh5DatePicker').classList.contains('active')) {
            document.getElementById('zyjh5DatePicker').classList.remove('active')
            document.getElementById('zyjh5DatePicker-mask').classList.remove('active');
        } else {
            document.getElementById('zyjh5DatePicker').classList.add('active');
            document.getElementById('zyjh5DatePicker-mask').classList.remove('active');
        }
    }


    (function (global, factory) {
        typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
            typeof define === 'function' && define.amd ? define(factory) :
                (global.zyjh5DatePicker = factory());
    })(window, (function () {
        'use strict';
        const days_CN = ['日', '一', '二', '三', '四', '五', '六'];
        const week = () => {
            var days = [];
            for (let index = 0; index < 7; index++) {
                if (index == 0 || index == 6) {
                    days.push(Element('div', { class: 'zyjh5DatePicker-day-item zyjh5DatePicker-day-weekend' }, {}, [days_CN[index]]))
                } else {
                    days.push(Element('div', { class: 'zyjh5DatePicker-day-item' }, {}, [days_CN[index]]))
                }
            }
            return days;
        }

        const getDate = (year, month) => {
            var d = new Date(year, month, 0);
            return d.getDate();
        }

        const getDay = (year, month) => {
            var d = new Date(year, month, 1);
            return d.getDay();
        }

        const getFull = (num, tmp_row) => {
            for (let j = 0; j < num; j++) {
                tmp_row.push(Element('div', { class: 'zyjh5DatePicker-date-colum' }, {}, [
                    Element('div', { class: 'zyjh5DatePicker-date-colum-wrap' }, {}, [
                        Element('div', { class: 'zyjh5DatePicker-date-colum-wrap-date' }, {}, []),
                        Element('div', { class: 'zyjh5DatePicker-date-colum-wrap-info' }, {}, []),
                    ]),
                ]))
            }
        }

        const date = () => {
            var tmp = [];
            var tmp_row = [];
            var d = new Date();
            let nd = d.getDate();
            var hdstr = (d.getMonth() + 1) < 10 ? ('0' + (d.getMonth() + 1)) : (d.getMonth() + 1);
            let dates = getDate(d.getFullYear(), d.getMonth());
            let day = getDay(d.getFullYear(), d.getMonth());
            getFull(day, tmp_row);
            for (let index = 0; index < dates; index++) {
                let classstr = 'zyjh5DatePicker-date-colum-wrap';
                if ((index + 1) === nd) {
                    classstr = 'zyjh5DatePicker-date-colum-wrap active';
                }
                let canc = true;
                if ((index + 1) < nd) {
                    classstr += ' cantchoose';
                    canc = false;
                }
                let odn = (index + 1) < 10 ? ('0' + (index + 1)) : (index + 1);
                let jr = holidays[hdstr + '-' + odn];
                let jrstatus = '';
                if (jr) {
                    if (jr.holiday) {
                        jrstatus = '休';
                    } else {
                        jrstatus = '班';
                    }
                }
                tmp_row.push(Element('div', { class: 'zyjh5DatePicker-date-colum' }, {}, [
                    Element('div', { class: (jrstatus == '休' ? (classstr + ' xiu') : (classstr + '')) }, canc ? { tap: choosedate } : {}, [
                        Element('div', { class: (jr ? 'zyjh5DatePicker-date-colum-wrap-status active' : 'zyjh5DatePicker-date-colum-wrap-status') }, {}, [jrstatus]),
                        Element('div', { class: 'zyjh5DatePicker-date-colum-wrap-date' }, {}, [index + 1]),
                        Element('div', { class: 'zyjh5DatePicker-date-colum-wrap-info' }, {}, [jr && jr.holiday ? jr.name : '¥228']),
                    ]),
                ]))
            }
            tmp_row = _.chunk(tmp_row, 7);
            getFull(7 - tmp_row[tmp_row.length - 1].length, tmp_row[tmp_row.length - 1]);
            _.forEach(tmp_row, function (arr) {
                tmp.push(Element('div', { class: 'zyjh5DatePicker-date-row' }, {}, arr));
            })
            return tmp;
        }
        const renderVirtualDom = () => {
            const tree = Element('div', { id: 'zyjh5DatePicker', class: 'zyjh5DatePicker' }, {}, [
                Element('div', { class: 'zyjh5DatePicker-title' }, {}, [
                    Element('span', {}, {}, ['请选择日期']),
                    Element('div', { class: 'zyjh5DatePicker-closebox' }, { tap: closezyjh5date }, [
                        Element('div', { class: 'zyjh5DatePicker-closebox-line1' }),
                        Element('div', { class: 'zyjh5DatePicker-closebox-line2' }),
                    ])
                ]),
                Element('div', { class: 'zyjh5DatePicker-day' }, {}, week()),
                Element('div', { class: 'zyjh5DatePicker-yearslide' }, {}, [
                    // Element('div', { class: 'zyjh5DatePicker-yearslide-iconleft' }),
                    Element('div', { class: 'zyjh5DatePicker-yearslide-text' }, {}, ['2019年2月']),
                    // Element('div', { class: 'zyjh5DatePicker-yearslide-iconright' }),
                ]),
                Element('div', { class: 'zyjh5DatePicker-date' }, {}, date()),
            ]);

            const root = tree.render();
            document.body.appendChild(root);
        };
        return zyjh5DatePicker => () => {
            renderVirtualDom();
        }
    }));
    zyjh5DatePicker()();
});